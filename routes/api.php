<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProdutoController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Use App\Models\Produto;
 

Route::get('produtos', function() {
    // If the Content-Type and Accept headers are set to 'application/json', 
    // this will return a JSON structure. This will be cleaned up later.
    return Produto::all();
});
 
Route::get('produtos/{id}', function($id) {
    return Produto::find($id);
});

Route::post('produtos', function(Request $request) {
    return Produto::create($request->all());
});

Route::put('produtos/{id}', function(Request $request, $id) {
    $Produto = Produto::findOrFail($id);
    $Produto->update($request->all());

    return $Produto;
});

Route::delete('produtos/{id}', function($id) {
    Produto::find($id)->delete();

    return 204;
});

/*
Route::get('produtos', 'ProdutoController@index');
Route::get('produtos/{id}', 'ProdutoController@show');
Route::post('produtos', 'ProdutoController@store');
Route::put('produtos/{id}', 'ProdutoController@update');
Route::delete('produtos/{id}', 'ProdutoController@delete');
*/