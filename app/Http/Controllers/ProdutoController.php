<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Produto;
 
class ProdutoController extends Controller
{
    public function index()
    {
        return Produto::all();
    }
 
    public function show($id)
    {
        return Produto::find($id);
    }

    public function store(Request $request)
    {
        return Produto::create($request->all());
    }

    public function update(Request $request, $id)
    {
        $Produto = Produto::findOrFail($id);
        $Produto->update($request->all());

        return $Produto;
    }

    public function delete(Request $request, $id)
    {
        $Produto = Produto::findOrFail($id);
        $Produto->delete();

        return 204;
    }
}

